class QuestionLike
  def self.all
    results = QuestionDatabase.instance.execute("SELECT * FROM question_likes")
    results.map { |result| QuestionLike.new(result) }
  end

  attr_accessor :id, :question_id, :user_id

  def initialize(options = {})
    @id = options["id"]
    @question_id = options["question_id"]
    @user_id = options["user_id"]
  end

  def create
    raise "already saved!" unless self.id.nil?

    QuestionDatabase.instance.execute(<<-SQL, question_id, user_id)
      INSERT INTO
        question_likes (question_id, user_id)
      VALUES
        (?, ?)
    SQL

    @id = QuestionDatabase.instance.last_insert_row_id
  end

  def self.find_by_id(id)
    results = QuestionDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        question_likes
      WHERE
        id = ?
    SQL

    QuestionLike.new(results.first)
  end

  def self.likers_for_question_id(question_id)
    likers = QuestionDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        u.id, u.fname, u.lname
      FROM
        question_likes ql JOIN users u ON ql.user_id = u.id
      WHERE
        ql.question_id = ?
    SQL

    likers.map { |liker_opts| User.new(liker_opts) }
  end

  def self.num_likes_for_question_id(question_id)
    like_counts = QuestionDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        count(user_id) like_count
      FROM
        question_likes
      WHERE
        question_id = ?
    SQL

    like_counts.first["like_count"]
  end

  def self.liked_questions_for_user_id(user_id)
    questions = QuestionDatabase.instance.execute(<<-SQL, user_id)
      SELECT
        q.id, q.title, q.body, q.user_id
      FROM
        question_likes ql JOIN questions q ON ql.question_id = q.id
      WHERE
        ql.user_id = ?
    SQL


    questions.map { |question_opts| Question.new(question_opts) }
  end

  def self.most_liked_questions(n)
    questions_by_likes = QuestionDatabase.instance.execute(<<-SQL)
      SELECT
        q.id, q.title, q.body, q.user_id
      FROM
        question_likes ql JOIN questions q ON ql.question_id = q.id
      GROUP BY
        ql.question_id
      ORDER BY
        count(ql.user_id) DESC
    SQL

    questions_by_likes.take(n).map do |question_opts|
       Question.new(question_opts)
     end
  end

end