class User
  include Save

  PARAMETER_NAMES =["users", "fname", "lname"]


  def self.all
    results = QuestionDatabase.instance.execute("SELECT * FROM users")
    results.map { |result| User.new(result) }
  end

  attr_accessor :id, :fname, :lname

  def initialize(options = {})
    @id = options["id"]
    @fname = options["fname"]
    @lname = options["lname"]
  end

  def save
    save_parameters = [self.fname, self.lname]
    generic_save(PARAMETER_NAMES.dup, save_parameters)
  end

  def self.find_by_id(id)
    results = QuestionDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        users
      WHERE
        id = ?
    SQL

    User.new(results.first)
  end

  def self.find_by_name(fname,lname)
    results = QuestionDatabase.instance.execute(<<-SQL, fname, lname)
      SELECT
        *
      FROM
        users
      WHERE
      fname = ? AND lname = ?
    SQL

    results.map { |result| User.new(result) }
  end

  def authored_questions
    Question.find_by_author_id(self.id)
  end

  def authored_replies
    Reply.find_by_user_id(self.id)
  end

  def followed_questions
    QuestionFollower.followed_questions_for_user_id(self.id)
  end

  def liked_questions
    QuestionLike.liked_questions_for_user_id(self.id)
  end

  def average_karma
    average_scores = QuestionDatabase.instance.execute(<<-SQL, self.id)
      SELECT
        COUNT (DISTINCT q.id) num_questions, COUNT(ql.user_id) num_likes
      FROM
        question_likes ql JOIN questions q ON q.id = ql.question_id
      WHERE
        q.user_id = ?


    SQL

    puts average_scores.first["num_likes"].to_f / average_scores.first["num_questions"]
  end

end