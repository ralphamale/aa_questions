CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(255) NOT NULL,
  lname VARCHAR(255) NOT NULL
);



CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  body VARCHAR(255) NOT NULL,
  user_id INTEGER NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE question_followers (
  id INTEGER PRIMARY KEY,
  question_id INTEGER NOT NULL,

  user_id INTEGER NOT NULL,
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (user_id) REFERENCES users(id)


);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  question_id INTEGER NOT NULL,

  parent_id INTEGER,


  user_id INTEGER NOT NULL,

  body VARCHAR(255) NOT NULL,
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY(user_id) REFERENCES users(id),
  FOREIGN KEY (parent_id) REFERENCES id
);

CREATE TABLE question_likes (
  question_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY(question_id) REFERENCES questions(id)

);

INSERT INTO
  users (fname, lname)
VALUES
('Ralph', 'N'),
('Thomas', 'C'),
('Sid', 'Sid'),
('Jenna', 'Jameson'),
('Milf', 'Hunter'),
('Martin', 'King'),
('Drake', 'NA');

INSERT INTO
  questions (title, body, user_id)
VALUES
  ('How are you?', 'Are you good or bad?', (SELECT id FROM users WHERE fname = 'Ralph')),
('What do you...?', 'Have?', 2),
('What is your favorite?', 'Web site?', 2);

INSERT INTO
  replies (question_id, parent_id, user_id, body)
VALUES
  ((SELECT id FROM questions WHERE title = 'How are you?'), NULL, (SELECT id FROM users WHERE fname = 'Thomas'), 'Great!');

INSERT INTO
  question_followers (question_id, user_id)
VALUES
((SELECT id FROM questions WHERE title = 'How are you?'), (SELECT id FROM users WHERE fname = 'Ralph')),
((SELECT id FROM questions WHERE title = 'How are you?'), (SELECT id FROM users WHERE fname = 'Thomas')),
(1, 6),
(1, 7),
(3, 3),
(3, 5),
(2, 5);

INSERT INTO
  question_likes (question_id, user_id)
VALUES
(1, 1),
(1, 2),
(1, 6),
(1, 7),
(3, 3),
(3, 5),
(2, 5) ;