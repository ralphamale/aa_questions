module Save

  def generic_save(parameter_names, save_parameters)
    table_name = parameter_names.shift

    if self.id.nil?
      question_marks = ("?" * save_parameters.count).split("").join(", ")
      QuestionDatabase.instance.execute(<<-SQL, *save_parameters)
        INSERT INTO
          #{table_name} (#{parameter_names.join(", ")})
        VALUES
          (#{question_marks})
      SQL

      @id = QuestionDatabase.instance.last_insert_row_id
    else #not nil
      set_string = parameter_names.map { |param| "#{param} = ?"}.join(", ")

      QuestionDatabase.instance.execute(<<-SQL, *save_parameters)
        UPDATE #{table_name}
        SET #{set_string}
        WHERE id = #{self.id}
      SQL
    end
  end

end