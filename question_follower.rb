require 'debugger'

class QuestionFollower
  def self.all
    results = QuestionDatabase.instance.execute("SELECT * FROM question_followers")
    results.map { |result| QuestionFollower.new(result) }
  end

  attr_accessor :id, :question_id, :user_id

  def initialize(options = {})
    @id = options["id"]
    @question_id = options["question_id"]
    @user_id = options["user_id"]
  end

  def create
    raise "already saved!" unless self.id.nil?

    QuestionDatabase.instance.execute(<<-SQL, question_id, user_id)
      INSERT INTO
        question_followers (question_id, user_id)
      VALUES
        (?, ?)
    SQL

    @id = QuestionDatabase.instance.last_insert_row_id
  end

  def self.find_by_id(id)
    results = QuestionDatabase.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        question_followers
      WHERE
        id = ?
    SQL

    QuestionFollower.new(results.first)
  end

  def self.followers_for_question_id(question_id)
    followers = QuestionDatabase.instance.execute(<<-SQL, question_id)
      SELECT
        u.id, u.fname, u.lname
      FROM
        question_followers qf JOIN users u ON qf.user_id = u.id
      WHERE
        qf.question_id = ?
    SQL

    followers.map { |follower_opts| User.new(follower_opts) }
  end

  def self.followed_questions_for_user_id(user_id)
    questions = QuestionDatabase.instance.execute(<<-SQL, user_id)
      SELECT
        q.id, q.title, q.body, q.user_id
      FROM
        question_followers qf JOIN questions q ON qf.question_id = q.id
      WHERE
        qf.user_id = ?
    SQL


    questions.map { |question_opts| Question.new(question_opts) }
  end

  def self.most_followed_questions(n)
    questions_by_followed = QuestionDatabase.instance.execute(<<-SQL)
      SELECT
        q.id, q.title, q.body, q.user_id
      FROM
        question_followers qf JOIN questions q ON qf.question_id = q.id
      GROUP BY
        qf.question_id
      ORDER BY
        count(qf.user_id) DESC
    SQL

    questions_by_followed.take(n).map do |question_opts|
       Question.new(question_opts)
     end
  end


end